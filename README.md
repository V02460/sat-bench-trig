# SAT Competition Benchmark – Trigonometric Functions

Benchmark familiy for the [SAT Competition][SATCOMP] verifying properties of
trigonometric function approximations. The implemented functions are sine,
cosine and tangent. All three functions are composed of a single partial cosine
implementation in the inverval [-π/2, π/2] for which three different methods of
approximation are provided:

- **BHASKARA** Bhaskara I’s [sine approximation formula][BHASKARA].
- **LINES** Piecewise linear approximation with a configurable number of line
  segments (5 and 17).
- **TAYLOR** Taylor series expansion with a configurable number of taylor terms
  (2, 4 and 6).

## Benchmark Generation

The `generator.bash` script is used to generate the CNF benchmark files from the
[C source file](sat-bench-trig.c). It requires [CBMC][CBMC] to be installed on
the system. To create all CNF files run the following commands.

```bash
./generator.bash -DTRIG_COS_BHASKARA > sat-bench-trig-bhaskara.cnf
./generator.bash -DTRIG_COS_LINES -DTRIG_COS_LINE_SEGMENT_N=5 > sat-bench-trig-lines5.cnf
./generator.bash -DTRIG_COS_LINES -DTRIG_COS_LINE_SEGMENT_N=17 > sat-bench-trig-lines17.cnf
./generator.bash -DTRIG_COS_TAYLOR -DTRIG_COS_TAYLOR_TERMS_N=2 > sat-bench-trig-taylor2.cnf
./generator.bash -DTRIG_COS_TAYLOR -DTRIG_COS_TAYLOR_TERMS_N=4 > sat-bench-trig-taylor4.cnf
./generator.bash -DTRIG_COS_TAYLOR -DTRIG_COS_TAYLOR_TERMS_N=6 > sat-bench-trig-taylor6.cnf
```

[BHASKARA]: <https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula>
[CBMC]: <https://github.com/diffblue/cbmc>
[SATCOMP]: <http://www.satcompetition.org>
