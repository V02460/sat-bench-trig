#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>


#define ARR_LEN(a) (sizeof(a)/sizeof(a[0]))
#define ARR_LAST(a) ((a)[ARR_LEN((a))-1])
#define ARR_FOREACH(a, body) for(size_t i=0; i<ARR_LEN((a)); i++) body
#define ARR_FOREACH_PAIR(a, body) for(size_t i=0, j=1; j<ARR_LEN((a)); i++, j++) body

bool is_between_open(float min, float x, float max) {
    return min < x && x < max;
}
bool is_between_closed(float min, float x, float max) {
    return min <= x && x <= max;
}
bool is_near(float x, float v, float e) {
    float d = x - v;
    return -e <= d && d <= e;
}

float nondet_float();
float nondet_between_open(float min, float max) {
    float r = nondet_float();
    __CPROVER_assume(is_between_open(min, r, max));
    return r;
}
float nondet_between_closed(float min, float max) {
    float r = nondet_float();
    __CPROVER_assume(is_between_closed(min, r, max));
    return r;
}


float cos_part(float x) {
    assert(is_between_closed(-M_PI_2, x, M_PI_2));

#ifdef TRIG_COS_TAYLOR
    float x2 = x * x;

    // Taylor series expansion.
    // Use an even number of taylor terms as their inaccuracies lead to f(PI/2) > 0.
    float r = 0.0;
    #if TRIG_COS_TAYLOR_TERMS_N >= 6
    r = x2 * (r + (1.0/479001600.0));
    r = x2 * (r - (1.0/3628800.0));
    #endif
    #if TRIG_COS_TAYLOR_TERMS_N >= 4
    r = x2 * (r + (1.0/40320.0));
    r = x2 * (r - (1.0/720.0));
    #endif
    #if TRIG_COS_TAYLOR_TERMS_N >= 2
    r = x2 * (r + (1.0/24.0));
    r = x2 * (r - (1.0/2.0));
    #endif

    return r + 1.0;

#elif defined(TRIG_COS_LINES)
    #if TRIG_COS_LINE_SEGMENT_N == 5
        #define _TRIG_COS_AS1 1.0054194
        #define _TRIG_COS_BS1 -0.12736896
        static const float as[] = {1.0, _TRIG_COS_AS1, 1.06930661, 1.19067734, 1.35429905, 1.52567402};
        static const float bs[] = {0.0, _TRIG_COS_BS1, -0.37707224, -0.61100686, -0.8158226, -0.96930506};
        static const float us[] = {(1.0 - _TRIG_COS_AS1) / _TRIG_COS_BS1, 0.25585252, 0.51882326, 0.79887273, 1.11657691};
    #elif TRIG_COS_LINE_SEGMENT_N == 17
        #define _TRIG_COS_AS1 1.00071168
        #define _TRIG_COS_BS1 -0.04619989
        static const float as[] = {1.0, _TRIG_COS_AS1, 1.00921298, 1.02599767, 1.05063307, 1.08247781, 1.12069107, 1.16424457, 1.2119373, 1.2624129, 1.31417934, 1.3656305, 1.4150707, 1.46073625, 1.5008367, 1.53352255, 1.55715278, 1.56936604};
        static const float bs[] = {0.0, _TRIG_COS_BS1, -0.1382055, -0.22903199, -0.31790447, -0.40406469, -0.48677758, -0.56533745, -0.63907406, -0.70735831, -0.76960766, -0.82529087, -0.87393345, -0.91511821, -0.94850194, -0.97376936, -0.99081862, -0.99907977};
        static const float us[] = {(1.0 - _TRIG_COS_AS1) / _TRIG_COS_BS1, 0.09239975, 0.18479949, 0.27719924, 0.36959898, 0.46199872, 0.55439845, 0.64679818, 0.7391979, 0.83159761, 0.9239973,  1.01639697, 1.10879659, 1.20119615, 1.29359552, 1.38599475, 1.47839099};
    #else
        #error "No data for given number of line segments"
    #endif
    assert(ARR_LEN(as) == ARR_LEN(bs) && ARR_LEN(bs) == ARR_LEN(us) + 1);
    ARR_FOREACH_PAIR(us, {
        assert(us[i] < us[j]);
    })

    x = fabsf(x);

    float a = ARR_LAST(as);
    float b = ARR_LAST(bs);
    ARR_FOREACH(us, {
        if (x < us[i]) {
            a = as[i];
            b = bs[i];
            break;
        }
    })
    return a + b * x;

#else
    // https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula
    float pi2 = (float)M_PI * (float)M_PI;
    float x2 = x * x;
    return (pi2 - 4.0 * x2) / (pi2 + x2);

#endif
}

float flt_cos(float x) {
    assert(is_between_closed(-M_PI, x, M_PI));
    x = fabsf(x);
    return x <= M_PI_2 ? cos_part(x) : -cos_part(M_PI - x);
}

float flt_sin(float x) {
    assert(is_between_closed(-M_PI, x, M_PI));
    return x > 0.0 ? cos_part(x - M_PI_2) : -cos_part(x + M_PI_2);
}

float flt_tan(float x) {
    assert(is_between_open(-M_PI_2, x, M_PI_2));
    return flt_sin(x) / flt_cos(x);
}

int main() {
    float e = 0.1;

    float angle = nondet_between_closed(-M_PI, M_PI);
    float cos_r = flt_cos(angle);
    float sin_r = flt_sin(angle);

    // Check range
    assert(is_between_closed(-1.0, cos_r, 1.0));
    assert(is_between_closed(-1.0, sin_r, 1.0));

    // Check sign
    assert(flt_cos(nondet_between_open(-M_PI, -M_PI_2)) <= 0.0);
    assert(flt_cos(nondet_between_open(-M_PI_2, M_PI_2)) >= 0.0);
    assert(flt_cos(nondet_between_open(M_PI_2, M_PI)) <= 0.0);

    assert(flt_sin(nondet_between_open(-M_PI, 0.0)) <= 0.0);
    assert(flt_sin(nondet_between_open(0.0, M_PI)) >= 0.0);

    assert(flt_tan(nondet_between_open(-M_PI_2, 0.0)) <= 0.0);
    assert(flt_tan(nondet_between_open(0.0, M_PI_2)) >= 0.0);

    // Check values
    assert(is_near(flt_cos(-M_PI), -1.0, e));
    assert(is_near(flt_cos(-M_PI_2), 0.0, e));
    assert(is_near(flt_cos(0.0), 1.0, e));
    assert(is_near(flt_cos(M_PI_2), 0.0, e));
    assert(is_near(flt_cos(M_PI), -1.0, e));

    assert(is_near(flt_sin(-M_PI), 0.0, e));
    assert(is_near(flt_sin(-M_PI_2), -1.0, e));
    assert(is_near(flt_sin(0.0), 0.0, e));
    assert(is_near(flt_sin(M_PI_2), 1.0, e));
    assert(is_near(flt_sin(M_PI), 0.0, e));

    assert(is_near(flt_tan(-M_PI_2 * 0.5), -1.0, e));
    assert(is_near(flt_tan(0.0), 0.0, e));
    assert(is_near(flt_tan(M_PI_2 * 0.5), 1.0, e));

    // Pythagorean identity
    assert(is_near(cos_r * cos_r + sin_r * sin_r, 1.0, e));

    return 0;
}
