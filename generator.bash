#!/bin/bash

FILE="sat-bench-trig.c"
COMMAND="cbmc --verbosity 0 --dimacs $FILE $*"

echo "c SAT Competition Benchmark"
echo "c Approximation of Trigonometric Functions for SAT-based Verification"
echo "c"
echo -n "c " && sha256sum --tag $FILE
echo "c"
echo "c $COMMAND"
echo "c"
$COMMAND
